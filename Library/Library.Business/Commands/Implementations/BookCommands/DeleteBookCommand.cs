﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Business.Commands.Implementations.BookCommands
{
    public class DeleteBookCommand : ICommand
    {
        public int BookId { get; set; }
    }
}
