﻿using Library.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Business.Commands.Implementations.BookCommands
{
    public class InsertBookCommand : ICommand
    {
        public DateTime ReleaseDate { get; set; }

        public string Author { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public int SubmittedBy { get; set; }

        public DateTime InsertDate { get; set; }

        public List<BookGenre> BookGenres { get; set; }

        public byte[] BookFile { get; set; }
    }
}
