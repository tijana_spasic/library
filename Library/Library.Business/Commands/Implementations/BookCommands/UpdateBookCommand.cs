﻿using System;

namespace Library.Business.Commands.Implementations.BookCommands
{
    public class UpdateBookCommand : ICommand
    {
        public int BookId { get; set; }
        public DateTime ReleaseDate { get; set; }

        public string Author { get; set; }

        public string Caption { get; set; }

        public int SubmittedBy { get; set; }
    }
}
