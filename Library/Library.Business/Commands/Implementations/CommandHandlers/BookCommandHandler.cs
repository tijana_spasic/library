﻿using Library.Business.Commands.Implementations.BookCommands;
using Library.Business.Extensions.Books;
using Library.Data.Models;
using Library.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Library.Business.Commands.Implementations.CommandHandlers
{
    public class BookCommandHandler :
        ICommandHandler<InsertBookCommand>,
        ICommandHandler<UpdateBookCommand>,
        ICommandHandler<DeleteBookCommand>
    {
        private readonly IWriteRepository<Book> _writeRepository;

        public BookCommandHandler(IWriteRepository<Book> writeRepository)
        {
            _writeRepository = writeRepository;
        }
        public async Task Handler(InsertBookCommand command)
        {
            var bookData = command.InsertBookCommandToData();

            await _writeRepository.InsertItemAsync(bookData);
        }

        public async Task Handler(UpdateBookCommand command)
        {
            var bookData = command.UpdateBookCommandToData();

            await _writeRepository.UpdateItemAsync(bookData);
        }

        public async Task Handler(DeleteBookCommand command)
        {
            var bookData = command.DeleteBookCommandToData();

            await _writeRepository.DeleteItemAsync(bookData);
        }
    }
}
