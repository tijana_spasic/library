﻿using System.Threading.Tasks;
using Library.Business.Commands.Implementations.UserCommands;
using Library.Business.Extensions.Users;
using Library.Data.Models;
using Library.Repository.Repository;
using Library.Repository.Users;

namespace Library.Business
{
    public class UserCommandHandler : 
        ICommandHandler<InsertUserCommand>,
        ICommandHandler<DeleteUserCommand>,
        ICommandHandler<UpdateUserCommand>
    {
        private readonly IWriteRepository<User> _writeRepository;

        public UserCommandHandler(IWriteRepository<User> writeRepository)
        {
            _writeRepository = writeRepository;
        }

        public async Task Handler(InsertUserCommand command)
        {
            var userData = command.InsertUserCommandToData();

            await _writeRepository.InsertItemAsync(userData);
        }

        public async Task Handler(DeleteUserCommand command)
        {
            var userData = command.DeleteUserCommandToData();

            await _writeRepository.DeleteItemAsync(userData);
        }

        public async Task Handler(UpdateUserCommand command)
        {
            var userData = command.UpdateUserCommandToData();

            await _writeRepository.UpdateItemAsync(userData);
        }
    }
}
