﻿using System;

namespace Library.Business
{
    public class InsertUserCommand : ICommand
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Address { get; set; }

        public int MaritalStatusId { get; set; }

        public int IdNumber { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public DateTime InsertDate { get; set; }
    }
}
