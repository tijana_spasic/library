﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Business.Commands.Implementations.UserCommands
{
    public class UpdateUserCommand : ICommand
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        public string Address { get; set; }

        public int MaritalStatusId { get; set; }

        public string PhoneNumber { get; set; }
    }
}
