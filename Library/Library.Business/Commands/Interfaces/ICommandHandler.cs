﻿using System.Threading.Tasks;

namespace Library.Business
{
    public interface ICommandHandler<in TCommand>
    where TCommand : ICommand
    {
        Task Handler(TCommand command);
    }
}
