﻿using Library.Business.Commands.Implementations.BookCommands;
using Library.Data.Models;

namespace Library.Business.Extensions.Books
{
    public static class BookCommandExtension
    {
        public static Book InsertBookCommandToData(this InsertBookCommand command)
        {
            return new Book
            {
                Author = command.Author,
                BookGenres = command.BookGenres,
                Caption = command.Caption,
                Description = command.Description,
                InsertDate = command.InsertDate,
                ReleaseDate = command.ReleaseDate,
                SubmittedBy = command.SubmittedBy,
                BookFile = command.BookFile
            };
        }

        public static Book UpdateBookCommandToData(this UpdateBookCommand command)
        {
            return new Book
            {
                BookId = command.BookId,
                Author = command.Author,
                Caption = command.Caption,
                ReleaseDate = command.ReleaseDate,
                SubmittedBy = command.SubmittedBy
            };
        }

        public static Book DeleteBookCommandToData(this DeleteBookCommand command)
        {
            return new Book
            {
                BookId = command.BookId
            };
        }
    }
}
