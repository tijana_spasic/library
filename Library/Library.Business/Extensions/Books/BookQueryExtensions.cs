﻿using Library.Business.Models.Books;
using Library.Common.Utils;
using Library.Data.Models;
using System;

namespace Library.Business.Extensions.Books
{
    public static class BookQueryExtensions
    {
        public static BookDto ToBookDto(this Book book)
        {
            var genres = Enums.GetValues<Common.Enums.Genre>();
            var genreNames = string.Empty;

            foreach (var bookGenre in book.BookGenres)
            {
                genreNames += Enum.GetName(typeof(Common.Enums.Genre), bookGenre.GenreId) + " ";
            }

            var bookDto = new BookDto
            {
                Author=book.Author,
                BookFile=book.BookFile,
                BookGenres=genreNames,
                BookId=book.BookId,
                Caption=book.Caption,
                DeleteDate=book.DeleteDate,
                Description=book.Description,
                InsertDate=book.InsertDate,
                ReleaseDate=book.ReleaseDate,
                SubmittedBy=book.SubmittedBy
            };

            return bookDto;
        }

    }
}
