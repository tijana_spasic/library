﻿using Library.Business.Commands.Implementations.UserCommands;
using Library.Data.Models;

namespace Library.Business.Extensions.Users
{
    public static class UserCommandExtension
    {
        public static User InsertUserCommandToData(this InsertUserCommand command)
        {
            var userData = new User
            {
                Address = command.Address,
                FirstName = command.FirstName,
                MaritalStatusId = command.MaritalStatusId,
                LastName = command.LastName,
                Email = command.Email,
                UserName = command.UserName,
                InsertDate = command.InsertDate,
                PhoneNumber = command.PhoneNumber
            };

            return userData;
        }

        public static User UpdateUserCommandToData(this UpdateUserCommand command)
        {
            var userData = new User
            {
                Id = command.Id,
                Address = command.Address,
                PhoneNumber=command.PhoneNumber,
                MaritalStatusId=command.MaritalStatusId
            };

            return userData;
        }

        public static User DeleteUserCommandToData(this DeleteUserCommand command)
        {
            var userData = new User
            {
                Id = command.Id
            };

            return userData;
        }
    }
}
