﻿using Library.Business.Extensions.Books;
using Library.Business.Models.Users;
using Library.Data.Models;
using System.Linq;

namespace Library.Business.Extensions.Users
{
    public static class UserQueryExtension
    {
        public static UserDto ToUserDto(this User user)
        {
            var userDto = new UserDto
            {
                Address = user.Address,
                DeleteDate = user.DeleteDate,
                FirstName = user.FirstName,
                Id = user.Id,
                InsertDate = user.InsertDate,
                LastName = user.LastName,
                MaritalStatusId = user.MaritalStatusId,
                PhoneNumber = user.PhoneNumber,
                Username = user.UserName
            };

            return userDto;
        }
    }
}
