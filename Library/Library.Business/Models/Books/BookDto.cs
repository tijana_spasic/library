﻿using Library.Business.Models.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Business.Models.Books
{
    public class BookDto
    {
        public int BookId { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string Author { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public int SubmittedBy { get; set; }

        public UserDto User { get; set; }

        public DateTime InsertDate { get; set; }

        public DateTime? DeleteDate { get; set; }

        public string BookGenres { get; set; }

        public byte[] BookFile { get; set; }
    }
}
