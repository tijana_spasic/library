﻿using Library.Business.Models.Books;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Business.Models.Users
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        public string Username { get; set; }

        public int MaritalStatusId { get; set; }

        public DateTime InsertDate { get; set; }

        public DateTime? DeleteDate { get; set; }

        public List<BookDto> Books { get; set; }
    }
}
