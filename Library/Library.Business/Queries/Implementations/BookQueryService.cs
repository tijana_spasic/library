﻿using Library.Business.Extensions.Books;
using Library.Business.Models.Books;
using Library.Business.Queries.Interfaces;
using Library.Data.Models;
using Library.Repository.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Library.Business.Queries.Implementations
{
    public class BookQueryService : IBookQueryService
    {
        private readonly IReadOnlyRepository<Book> _readOnlyRepository;

        public BookQueryService(IReadOnlyRepository<Book> readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }

        public IEnumerable<BookDto> GetAllBooks()
        {
            var booksDto = _readOnlyRepository.GetQuery(item => item.DeleteDate == null)
                                      .Include(book => book.BookGenres)
                                      .ThenInclude(book => book.Genre)
                                      .Select(book => book.ToBookDto());
            return booksDto;
        }

        public BookDto GetBookById(int id)
        {
            return _readOnlyRepository.GetQuery(item => item.DeleteDate == null && item.BookId == id)
                                      .Include(book => book.BookGenres)
                                      .ThenInclude(book => book.Genre)
                                      .Select(book => book.ToBookDto())
                                      .SingleOrDefault();
        }

        public IEnumerable<BookDto> GetBookByName(string name)
        {
            return _readOnlyRepository.GetQuery(item => item.DeleteDate == null && item.Caption.ToLower().Contains(name.ToLower()))
                                      .Include(book => book.BookGenres)
                                      .ThenInclude(book => book.Genre)
                                      .Select(book => book.ToBookDto());
        }
    }
}
