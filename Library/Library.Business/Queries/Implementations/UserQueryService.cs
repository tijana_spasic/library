﻿using Library.Business.Extensions.Users;
using Library.Business.Models.Users;
using Library.Business.Queries.Interfaces;
using Library.Data.Models;
using Library.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Business.Queries.Implementations
{
    public class UserQueryService : IUserQueryService
    {
        private readonly IReadOnlyRepository<User> _readOnlyRepository;

        public UserQueryService(IReadOnlyRepository<User> readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }

        public IEnumerable<UserDto> GetAllUsers()
        {
            return _readOnlyRepository.GetQuery(item => item.DeleteDate == null)
                                      .Select(user=>user.ToUserDto());
        }

        public UserDto GetUserById(int id)
        {
            return _readOnlyRepository.GetQuery(item => item.DeleteDate == null && item.Id == id)
                                      .Select(user => user.ToUserDto())
                                      .SingleOrDefault();
        }

        public UserDto GetUserByUsername(string username)
        {
            return _readOnlyRepository.GetQuery(item => item.DeleteDate == null && item.UserName == username)
                                      .Select(user => user.ToUserDto())
                                      .SingleOrDefault();
        }
    }
}
