﻿using Library.Business.Models.Books;
using System.Collections.Generic;

namespace Library.Business.Queries.Interfaces
{
    public interface IBookQueryService
    {
        IEnumerable<BookDto> GetAllBooks();
        BookDto GetBookById(int id);
        IEnumerable<BookDto> GetBookByName(string name);
    }
}
