﻿using Library.Business.Models.Users;
using System.Collections.Generic;

namespace Library.Business.Queries.Interfaces
{
    public interface IUserQueryService
    {
        IEnumerable<UserDto> GetAllUsers();
        UserDto GetUserById(int id);
        UserDto GetUserByUsername(string username);
    }
}
