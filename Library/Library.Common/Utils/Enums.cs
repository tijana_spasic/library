﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Common.Utils
{
    public static class Enums
    {
        public static IEnumerable<TEnum> GetValues<TEnum>()
            where TEnum : Enum
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
        }
    }
}