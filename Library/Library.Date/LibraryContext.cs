﻿using Library.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Library.Data
{
    public class LibraryContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options) { }

        public DbSet<Book> Books { get; set; }

        public DbSet<MaritalStatus> MaritalStatus { get; set; }

        public DbSet<Genre> Genre { get; set; }

        public DbSet<BookGenre> BookGenre { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MaritalStatus>()
              .Property(p => p.Id)
              .ValueGeneratedNever();

            modelBuilder.Entity<Book>()
                .HasOne(b => b.User)
                .WithMany(u => u.Books)
                .HasForeignKey(b => b.SubmittedBy);

            modelBuilder.Entity<Book>()
                .HasMany(b => b.BookGenres);

            modelBuilder.Entity<User>()
              .HasKey(model => model.Id);

            modelBuilder.Entity<Genre>()
                .Property(genre => genre.GenreId)
                .ValueGeneratedNever();

            modelBuilder.Entity<BookGenre>()
                .HasKey(bg => new { bg.BookId, bg.GenreId });

            modelBuilder.Entity<BookGenre>()
                .HasOne(bg => bg.Book)
                .WithMany(b => b.BookGenres)
                .HasForeignKey(bg => bg.BookId);

            modelBuilder.Entity<BookGenre>()
                .HasOne(bg => bg.Genre)
                .WithMany(g => g.BookGenres)
                .HasForeignKey(bg => bg.GenreId);
        }
    }
}

