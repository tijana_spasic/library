﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Data.Models
{
    public class Book
    {
        public int BookId { get; set; }

        public string Caption { get; set; }

        public string Author { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string Description { get; set; }

        public int SubmittedBy { get; set; } 

        public User User { get; set; }

        public DateTime InsertDate { get; set; }

        public DateTime? DeleteDate { get; set; }

        public List<BookGenre> BookGenres { get; set; }

        public byte[] BookFile { get; set; }
    }
}
