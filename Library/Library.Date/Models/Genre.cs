﻿using System.Collections.Generic;

namespace Library.Data.Models
{
    public class Genre
    {
        public int GenreId { get; set; }

        public string Caption { get; set; }

        public List<BookGenre> BookGenres { get; set; }
    }
}
