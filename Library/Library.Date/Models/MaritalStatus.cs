﻿namespace Library.Data.Models
{
    public class MaritalStatus
    {
        public int Id { get; set; }

        public string Caption { get; set; }
    }
}
