﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Library.Data.Models
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Address { get; set; }

        public int MaritalStatusId { get; set; }

        public DateTime InsertDate { get; set; }
        
        public DateTime? DeleteDate { get; set; }

        public List<Book> Books { get; set; }
    }
}
