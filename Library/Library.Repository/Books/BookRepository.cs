﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.Data;
using Library.Data.Models;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace Library.Repository.Books
{
    public class BookRepository : IBookRepository, IDisposable
    {
        private LibraryContext _context;

        public BookRepository(LibraryContext context)
        {
            this._context = context;
        }

        public async Task DeleteBookAsync(int bookId)
        {
            await this._context.Books
               .Where(bookDb => bookDb.BookId == bookId)
               .UpdateAsync(bookDb => new Book
               {
                   DeleteDate = DateTime.Now
               });
        }

        public List<Book> GetBooksAsync()
        {
            return _context.Books
               .Include(book => book.BookGenres
               .Select(bg=>new Genre
               {
                   GenreId=bg.GenreId,
                   Caption=bg.Genre.Caption
               }))
                .ToList();
        }

        public async Task<Book> GetBookByIDAsync(int bookId)
        {
            return await _context.Books.Where(book => book.BookId == bookId).SingleOrDefaultAsync();
        }

        public async Task InsertBookAsync(Book book)
        {
            await _context.Books.AddAsync(book);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateBookAsync(Book book)
        {
            await this._context.Books
               .Where(bookDb => bookDb.BookId == book.BookId)
               .UpdateAsync(bookId => new Book
               {
                   Caption = book.Caption,
                   Author = book.Author,
                   ReleaseDate = book.ReleaseDate
               });
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
