﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Library.Repository.Repository
{
    public interface IReadOnlyRepository<TEntity>
    {
        IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate);
    }
}
