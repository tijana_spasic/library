﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Library.Repository.Repository
{
    public interface IWriteRepository<TEntity> 
    {
        Task InsertItemAsync(TEntity item);
        Task UpdateItemAsync(TEntity item);
        Task DeleteItemAsync(TEntity item);
        IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate);

    }
}
