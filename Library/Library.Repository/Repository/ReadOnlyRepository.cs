﻿using Library.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Library.Repository.Repository
{
    public class ReadOnlyRepository<TEntity> : IReadOnlyRepository<TEntity>
        where TEntity : class

    {
        private LibraryContext _context = null;
        private DbSet<TEntity> table = null;

        public ReadOnlyRepository(LibraryContext context)
        {
            _context = context;
            table = _context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetQuery(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null)
        {
            return table.Where(predicate)
                        .AsNoTracking();
        }
    }
}
