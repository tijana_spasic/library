﻿using Library.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Repository.Users
{
    public interface IUserRepository : IDisposable
    {
        Task<IEnumerable<User>> GetUsersAsync();
        Task<User> GetUserByUsernameAsync(string username);
        Task<User> GetUserByIDAsync(int userId);
        Task InsertUserAsync(User user);
        Task DeleteUserAsync(int userID);
        Task UpdateUserAsync(User user);
    }
}
