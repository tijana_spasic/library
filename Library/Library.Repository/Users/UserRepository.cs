﻿using System;
using System.Collections.Generic;
using Library.Data.Models;
using Library.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Library.Repository.Users
{
    public class UserRepository : IUserRepository, IDisposable
    {
        private LibraryContext _context;

        public UserRepository(LibraryContext context)
        {
            this._context = context;
        }

        public async Task DeleteUserAsync(int userID)
        {
            await this._context.Users
               .Where(userDb => userDb.Id == userID)
               .UpdateAsync(userDb => new User
               {
                   DeleteDate=DateTime.Now
               });
        }

        public async Task<User> GetUserByIDAsync(int userId)
        {
            return await _context.Users.Where(user => user.Id == userId)
                                        .Include(user=>user.Books)
                                        .SingleOrDefaultAsync();
        }

        public async Task<User> GetUserByUsernameAsync(string username)
        {
            return await _context.Users.Where(user => user.UserName == username).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task InsertUserAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(User user)
        {
            await this._context.Users
               .Where(userDb => userDb.Id == user.Id)
               .UpdateAsync(userDb => new User
               {
                   PhoneNumber = user.PhoneNumber,
                   Address = user.Address,
                   MaritalStatusId = user.MaritalStatusId
               });
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
