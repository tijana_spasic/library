﻿using Library.Business;
using Library.Business.Commands.Implementations.BookCommands;
using Library.Business.Queries.Interfaces;
using Library.Extensions;
using Library.Models.Books;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Controllers
{
    [Authorize]
    public class BookController : Controller
    {
        private readonly IBookQueryService _bookQueryService;
        private readonly ICommandHandler<InsertBookCommand> _insertBookCommandHandler;
        private readonly ICommandHandler<UpdateBookCommand> _updateBookCommandHandler;
        private readonly ICommandHandler<DeleteBookCommand> _deleteBookCommandHandler;


        public BookController(IBookQueryService bookQueryService,
            ICommandHandler<InsertBookCommand> insertBookCommandHandler,
            ICommandHandler<UpdateBookCommand> updateBookCommandHandler,
            ICommandHandler<DeleteBookCommand> deleteBookCommandHandler)
        {
            _bookQueryService = bookQueryService;
            _insertBookCommandHandler = insertBookCommandHandler;
            _updateBookCommandHandler = updateBookCommandHandler;
            _deleteBookCommandHandler = deleteBookCommandHandler;

        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Books()
        {
            var booksVM = _bookQueryService.GetAllBooks()
                                           .Select(bookDto => bookDto.ToBookVM());
            return View(booksVM);
        }

        public IActionResult AddBook()
        {
            var book = BookVM.GetBookModelWithGenres();
            return View(book);
        }

        [ValidFilter]
        [HttpPost]
        public async Task<IActionResult> AddBook(BookVM book)
        {
            var insertBookCommand = book.ToInsertBookCommand();
            insertBookCommand.SubmittedBy = HttpContext.User.GetUserId();

            await _insertBookCommandHandler.Handler(insertBookCommand);

            return RedirectToAction(nameof(Books));
        }

        public ActionResult EditBook(int id)
        {
            var bookVM = _bookQueryService.GetBookById(id)
                                          .ToBookVM();
            return View(bookVM);
        }

        [ValidFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBook(BookVM bookVM)
        {
            var updateBookCommand = bookVM.ToUpdateBookCommand();
            await _updateBookCommandHandler.Handler(updateBookCommand);

            return RedirectToAction(nameof(Books));
        }

        public ActionResult DeleteBook()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteBook(int id)
        {
            var book = _bookQueryService.GetBookById(id);

            var deleteBookCommandHandler = new DeleteBookCommand
            {
                BookId = book.BookId
            };

            await _deleteBookCommandHandler.Handler(deleteBookCommandHandler);

            return RedirectToAction(nameof(Books));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SearchBook(string SearchName)
        {
            var booksVM = _bookQueryService.GetBookByName(SearchName)
                                           .Select(book => book.ToBookVM());

            return View(nameof(Books), booksVM);
        }

        public IActionResult DownloadBook(int id)
        {
            var book = _bookQueryService.GetBookById(id);

            if (book.BookFile == null)
            {
                return View(nameof(NoPreview));
            }

            return File(book.BookFile, "application/pdf");
        }

        public IActionResult BookDetails(int id)
        {
            var bookVM = _bookQueryService.GetBookById(id)
                                          .ToBookVM();
            return View(bookVM);
        }

        public ActionResult NoPreview()
        {
            return View();
        }
    }

}
