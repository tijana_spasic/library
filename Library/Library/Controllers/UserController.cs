﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Library.Business;
using Library.Business.Commands.Implementations.UserCommands;
using Library.Business.Queries.Interfaces;
using Library.Common.Enums;
using Library.Common.Utils;
using Library.Extensions;
using Library.Models.Users;
using Library.Resources;
using Microsoft.AspNetCore.Mvc;

namespace Library.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserQueryService _userQueryService;
        private readonly IBookQueryService _bookQueryService;
        private readonly ICommandHandler<InsertUserCommand> _insertUserCommandHandler;
        private readonly ICommandHandler<DeleteUserCommand> _deleteUserCommandHandler;
        private readonly ICommandHandler<UpdateUserCommand> _updateUserCommandHandler;

        public UserController(
            IUserQueryService userQueryService,
            IBookQueryService bookQueryService,
            ICommandHandler<InsertUserCommand> insertUserCommandHandler,
            ICommandHandler<DeleteUserCommand> deleteUserCommandHandler,
            ICommandHandler<UpdateUserCommand> updateUserCommandHandler)
        {
            _userQueryService = userQueryService;
            _bookQueryService = bookQueryService;
            _insertUserCommandHandler = insertUserCommandHandler;
            _deleteUserCommandHandler = deleteUserCommandHandler;
            _updateUserCommandHandler = updateUserCommandHandler;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Users()
        {
            var usersVM = _userQueryService.GetAllUsers()
                                           .Select(user => user.ToUserVMData());

            return View(usersVM);
        }

        public IActionResult AddUser()
        {
            var user = UserVM.GetUserWithMaritals();
            return View(user);
        }

        [ValidFilter]
        [HttpPost]
        public async Task<IActionResult> AddUser(UserVM userVM)
        {
            var existingUser = _userQueryService.GetUserByUsername(userVM.UserName);

            if (existingUser != null)
            {
                ViewBag.Message = LibraryControllerMessages.UserNameExists;
                var maritalValues = Enums.GetValues<Maritial>();
                var newList = maritalValues.ToSelectList();
                userVM.MaritalStatuses = newList;
                userVM.UserName = String.Empty;

                return View(userVM);
            }

            var insertUserCommand = userVM.ToInsertUserCommand();

            await _insertUserCommandHandler.Handler(insertUserCommand);

            return RedirectToAction(nameof(Users));
        }

        public ActionResult EditUser(int id)
        {
            var userVM = _userQueryService.GetUserById(id)
                                          .ToEditUserVMData();

            var maritalValues = Enums.GetValues<Maritial>();
            var maritalSelectList = maritalValues.ToSelectList();
            userVM.MaritalStatuses = maritalSelectList;

            return View(userVM);
        }


        [ValidFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUser(EditUserVM editUserVM)
        {
            var updateUserCommand = editUserVM.ToUpdateUserCommand();
            await _updateUserCommandHandler.Handler(updateUserCommand);

            return RedirectToAction(nameof(Users));
        }

        public ActionResult DeleteUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUser(int id)
        {
            var user = _userQueryService.GetUserById(id);

            var deleteUserCommand = new DeleteUserCommand
            {
                Id = id
            };

            await _deleteUserCommandHandler.Handler(deleteUserCommand);

            return RedirectToAction(nameof(Users));
        }

        public ActionResult DetailsUser(int id)
        {
            var booksVM = _bookQueryService.GetAllBooks()
                                           .Where(book => book.SubmittedBy == id)
                                           .Select(book => book.ToBookVM());

            return View(booksVM);
        }

    }
}