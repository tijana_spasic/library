﻿using Library.Models.BaseModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using System.Linq;

namespace Library.Controllers
{
    public class ValidFilterAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Debug.WriteLine("***(Logging Filter)Action Executing: " +
            filterContext.ActionDescriptor.DisplayName);

            var isValid = filterContext.ModelState.IsValid;
            if (!isValid)
            {
                foreach (var argument in filterContext.ActionArguments.Values.Where(v => v is BaseViewModel))
                {
                    var model = argument as BaseViewModel;
                    filterContext.Result = new ViewResult();
                }
            }
            base.OnActionExecuting(filterContext);
        }


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Debug.WriteLine("***(Logging Filter)Exception thrown***");
            base.OnActionExecuted(filterContext);
        }
    }
}
