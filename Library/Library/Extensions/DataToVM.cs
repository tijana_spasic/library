﻿using Library.Business.Models.Books;
using Library.Business.Models.Users;
using Library.Data.Models;
using Library.Models.Books;
using Library.Models.Users;

namespace Library.Extensions
{
    public static class DataToVM
    {
        public static UserVM ToUserVMData(this UserDto user)
        {
            var userVM =
                new UserVM
                {
                    UserId = user.Id,
                    UserName = user.Username,
                    Address = user.Address,
                    MaritalStatusId = user.MaritalStatusId,
                    PhoneNumber = user.PhoneNumber
                };

            return userVM;
        }

        public static EditUserVM ToEditUserVMData(this UserDto user)
        {
            var userVM =
                new EditUserVM
                {
                    UserId = user.Id,
                    UserName = user.Username,
                    Address = user.Address,
                    MaritalStatusId = user.MaritalStatusId,
                    PhoneNumber = user.PhoneNumber
                };

            return userVM;
        }

        public static BookVM ToBookVM(this BookDto book)
        {
            var bookVM =
                new BookVM
                {
                    Caption = book.Caption,
                    Description = book.Description,
                    Author = book.Author,
                    ReleaseDate = book.ReleaseDate,
                    Genres = book.BookGenres,
                    SubmittedBy = book.SubmittedBy,
                    BookId = book.BookId
                    //User = book.User.FirstName + " " + book.User.LastName
                };

            return bookVM;
        }
    }
}
