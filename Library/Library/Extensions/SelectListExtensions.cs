﻿using Library.Common.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Library.Extensions
{
    public static class SelectListExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectList(this IEnumerable<Maritial> maritals)
        {
            var maritalSelectList = maritals.Select(item =>
            new SelectListItem
            {
                Value = ((int)item).ToString(),
                Text = item.ToString()
            });
            return maritalSelectList;
        }

        public static IEnumerable<SelectListItem> ToSelectList(this IEnumerable<Genre> genres)
        {
            var genreSelectList = genres.Select(item =>
            new SelectListItem
            {
                Value = ((int)item).ToString(),
                Text = item.ToString()
            });

            return genreSelectList;
        }
    }
}