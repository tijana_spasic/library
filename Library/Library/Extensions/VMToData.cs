﻿using Library.Business;
using Library.Business.Commands.Implementations.BookCommands;
using Library.Business.Commands.Implementations.UserCommands;
using Library.Data.Models;
using Library.Models.Books;
using Library.Models.Users;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace Library.Extensions
{
    public static class VMToData
    {

        public static User ToUserData(this UserVM user)
        {
            var userDb = new User
            {
                Id = user.UserId,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address,
                MaritalStatusId = user.MaritalStatusId,
                InsertDate = DateTime.UtcNow,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber,
                Email = user.Email
            };

            return userDb;
        }

        public static InsertUserCommand ToInsertUserCommand(this UserVM userVM)
        {
            var insertUserCommand = new InsertUserCommand
            {
                FirstName = userVM.FirstName,
                LastName = userVM.LastName,
                Address = userVM.Address,
                MaritalStatusId = userVM.MaritalStatusId,
                IdNumber = userVM.UserId,
                PhoneNumber = userVM.PhoneNumber,
                InsertDate = DateTime.UtcNow,
                UserName = userVM.UserName,
                Email = userVM.Email
            };

            return insertUserCommand;
        }

        public static User ToEditUserData(this EditUserVM user)
        {
            var userDb = new User
            {
                Id = user.UserId,
                Address = user.Address,
                MaritalStatusId = user.MaritalStatusId,
                InsertDate = DateTime.UtcNow,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber,
            };

            return userDb;
        }

        public static UpdateUserCommand ToUpdateUserCommand(this EditUserVM userVM)
        {
            var updateUserCommand = new UpdateUserCommand
            { 
                Id=userVM.UserId,
                Address=userVM.Address,
                PhoneNumber=userVM.PhoneNumber,
                UserName=userVM.UserName,
                MaritalStatusId=userVM.MaritalStatusId
            };

            return updateUserCommand;
        }

        public static Book ToBookData(this BookVM book)
        {
            var bookDb = new Book
            {
                Author = book.Author,
                Caption = book.Caption,
                InsertDate = DateTime.UtcNow,
                ReleaseDate = book.ReleaseDate,
                Description = book.Description
            };

            bookDb.BookGenres = new List<BookGenre>();

            foreach (var genreId in book.SelectedBookGenres)
            {
                bookDb.BookGenres.Add(new BookGenre
                {
                    BookId = book.BookId,
                    GenreId = genreId
                });
            }

            using (var memoryStream = new MemoryStream())
            {
                book.BookFile.CopyTo(memoryStream);
                bookDb.BookFile = memoryStream.ToArray();
            };

            return bookDb;
        }

        public static UpdateBookCommand ToUpdateBookCommand(this BookVM bookVM)
        {
            var updateBookCommand = new UpdateBookCommand
            {
                BookId=bookVM.BookId,
                Author = bookVM.Author,
                Caption = bookVM.Caption,
                ReleaseDate = bookVM.ReleaseDate,
                SubmittedBy=bookVM.SubmittedBy
            };

            return updateBookCommand;
        }

        public static InsertBookCommand ToInsertBookCommand(this BookVM bookVM)
        {
            var insertBookCommand = new InsertBookCommand
            {
                Author = bookVM.Author,
                Caption = bookVM.Caption,
                Description = bookVM.Description,
                InsertDate = DateTime.UtcNow,
                ReleaseDate = bookVM.ReleaseDate,
            };

            insertBookCommand.BookGenres = new List<BookGenre>();

            foreach (var genreId in bookVM.SelectedBookGenres)
            {
                insertBookCommand.BookGenres.Add(new BookGenre
                {
                    BookId = bookVM.BookId,
                    GenreId = genreId
                });
            }

            using (var memoryStream = new MemoryStream())
            {
                if(bookVM.BookFile == null)
                {
                    insertBookCommand.BookFile = new byte[0];
                }
                else
                {
                    bookVM.BookFile.CopyTo(memoryStream);
                    insertBookCommand.BookFile = memoryStream.ToArray();
                }
            };

            return insertBookCommand;
        }
    }
}
