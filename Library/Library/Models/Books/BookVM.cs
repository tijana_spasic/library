﻿using Library.Common.Enums;
using Library.Common.Utils;
using Library.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Models.Books
{
    public class BookVM
    {
        public int BookId { get; set; }

        [Display(Name = "Title")]
        public string Caption { get; set; }

        public string Author { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }

        public string Description { get; set; }

        public int SubmittedBy { get; set; }

        public string User { get; set; }

        public string Genres { get; set; }

        [Display(Name = "Book Genres")]
        public IEnumerable<SelectListItem> BookGenres { get; set; }

        public List<int> SelectedBookGenres { get; set; }

        [Display(Name = "Upload book")]
        public IFormFile BookFile { get; set; }

        public string SearchName { get; set; }


        public BookVM()
        {

        }

        public BookVM(int bookId)
        {
            BookId = bookId;
        }

        public static BookVM GetBookModelWithGenres()
        {
            return new BookVM
            {
                BookGenres = Enums.GetValues<Genre>().ToSelectList()
            };
        }
    }
}
