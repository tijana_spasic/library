﻿using Library.Models.BaseModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Library.Models.Users
{
    public class EditUserVM : BaseViewModel
    {
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public int UserId { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        [Required]
        public int MaritalStatusId { get; set; }

        [Display(Name = "Marital Status")]
        public IEnumerable<SelectListItem> MaritalStatuses { get; set; }
    }
}
