﻿using Library.Business;
using Library.Business.Commands.Implementations.BookCommands;
using Library.Business.Commands.Implementations.CommandHandlers;
using Library.Business.Commands.Implementations.UserCommands;
using Library.Business.Queries.Implementations;
using Library.Business.Queries.Interfaces;
using Library.Data;
using Library.Data.Models;
using Library.Repository.Repository;
using Library.Repository.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Library
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddDbContext<LibraryContext>(options => options.UseSqlServer(Configuration.GetConnectionString("LibraryContextDb")));

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IBookQueryService, BookQueryService>();
            services.AddScoped<IUserQueryService, UserQueryService>();
            services.AddScoped<IWriteRepository<User>, WriteRepository<User>>();
            services.AddScoped<IWriteRepository<Book>, WriteRepository<Book>>();
            services.AddScoped<IReadOnlyRepository<User>, ReadOnlyRepository<User>>();
            services.AddScoped<IReadOnlyRepository<Book>, ReadOnlyRepository<Book>>();
            services.AddScoped<ICommandHandler<InsertUserCommand>, UserCommandHandler>();
            services.AddScoped<ICommandHandler<InsertBookCommand>, BookCommandHandler>();
            services.AddScoped<ICommandHandler<DeleteUserCommand>, UserCommandHandler>();
            services.AddScoped<ICommandHandler<DeleteBookCommand>, BookCommandHandler>();
            services.AddScoped<ICommandHandler<UpdateBookCommand>, BookCommandHandler>();
            services.AddScoped<ICommandHandler<UpdateUserCommand>, UserCommandHandler>();

            services.AddIdentity<Library.Data.Models.User, IdentityRole<int>>()
              .AddEntityFrameworkStores<LibraryContext>()
              .AddDefaultUI()
              .AddDefaultTokenProviders();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                 .AddRazorPagesOptions(options =>
                 {
                     options.AllowAreas = true;
                     options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Manage");
                     options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
                 });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Library}/{action=Index}/{id?}");
            });
        }
    }
}
