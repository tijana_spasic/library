#pragma checksum "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "bd9c47e443466d269423a0b43451d84a66492e82"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Book_BookDetails), @"mvc.1.0.view", @"/Views/Book/BookDetails.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Book/BookDetails.cshtml", typeof(AspNetCore.Views_Book_BookDetails))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\_ViewImports.cshtml"
using Library;

#line default
#line hidden
#line 2 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\_ViewImports.cshtml"
using Library.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bd9c47e443466d269423a0b43451d84a66492e82", @"/Views/Book/BookDetails.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dadb7a731bfbb305c411bc5eb7a307dbd6008a89", @"/Views/_ViewImports.cshtml")]
    public class Views_Book_BookDetails : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Library.Models.Books.BookVM>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(36, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
  
    ViewData["Title"] = "BookDetails";

#line default
#line hidden
            BeginContext(85, 128, true);
            WriteLiteral("\r\n<h2>Book Details</h2>\r\n\r\n<hr />\r\n<div class=\"row\">\r\n    <div class=\"col-md-4\">\r\n        <div class=\"form-group\">\r\n            ");
            EndContext();
            BeginContext(214, 37, false);
#line 13 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.LabelFor(model => model.Caption));

#line default
#line hidden
            EndContext();
            BeginContext(251, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(266, 105, false);
#line 14 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.TextBoxFor(model => model.Caption, new { @class = "form-control disabled", @readonly = "readonly" }));

#line default
#line hidden
            EndContext();
            BeginContext(371, 64, true);
            WriteLiteral("\r\n        </div>\r\n        <div class=\"form-group\">\r\n            ");
            EndContext();
            BeginContext(436, 36, false);
#line 17 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.LabelFor(model => model.Genres));

#line default
#line hidden
            EndContext();
            BeginContext(472, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(487, 104, false);
#line 18 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.TextBoxFor(model => model.Genres, new { @class = "form-control disabled", @readonly = "readonly" }));

#line default
#line hidden
            EndContext();
            BeginContext(591, 64, true);
            WriteLiteral("\r\n        </div>\r\n        <div class=\"form-group\">\r\n            ");
            EndContext();
            BeginContext(656, 36, false);
#line 21 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.LabelFor(model => model.Author));

#line default
#line hidden
            EndContext();
            BeginContext(692, 14, true);
            WriteLiteral("\r\n            ");
            EndContext();
            BeginContext(707, 95, false);
#line 22 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.TextBoxFor(model => model.Author, new { @class = "form-control", @readonly = "readonly" }));

#line default
#line hidden
            EndContext();
            BeginContext(802, 64, true);
            WriteLiteral("\r\n        </div>\r\n        <div class=\"form-group\">\r\n            ");
            EndContext();
            BeginContext(867, 39, false);
#line 25 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
       Write(Html.LabelFor(model=>model.Description));

#line default
#line hidden
            EndContext();
            BeginContext(906, 105, true);
            WriteLiteral("\r\n              <textarea rows=\"10\" cols=\"45\" readonly=\"readonly\" class=\"form-control\">\r\n                ");
            EndContext();
            BeginContext(1012, 17, false);
#line 27 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
           Write(Model.Description);

#line default
#line hidden
            EndContext();
            BeginContext(1029, 74, true);
            WriteLiteral("\r\n            </textarea>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1104, 69, false);
#line 33 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
Write(Html.ActionLink("Preview", "DownloadBook", new { id = Model.BookId }));

#line default
#line hidden
            EndContext();
            BeginContext(1173, 21, true);
            WriteLiteral("\r\n</div>\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1195, 45, false);
#line 36 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
Write(Html.ActionLink("Back to Book list", "Books"));

#line default
#line hidden
            EndContext();
            BeginContext(1240, 21, true);
            WriteLiteral("\r\n</div>\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1262, 55, false);
#line 39 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Book\BookDetails.cshtml"
Write(Html.ActionLink("Back to Home page", "Index","Library"));

#line default
#line hidden
            EndContext();
            BeginContext(1317, 8, true);
            WriteLiteral("\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Library.Models.Books.BookVM> Html { get; private set; }
    }
}
#pragma warning restore 1591
