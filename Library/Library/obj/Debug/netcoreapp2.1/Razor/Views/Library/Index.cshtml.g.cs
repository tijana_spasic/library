#pragma checksum "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Library\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d0cc6f1338d9fe464f45224ac4f8cbf21fa0b5d9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Library_Index), @"mvc.1.0.view", @"/Views/Library/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Library/Index.cshtml", typeof(AspNetCore.Views_Library_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\_ViewImports.cshtml"
using Library;

#line default
#line hidden
#line 2 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\_ViewImports.cshtml"
using Library.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d0cc6f1338d9fe464f45224ac4f8cbf21fa0b5d9", @"/Views/Library/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dadb7a731bfbb305c411bc5eb7a307dbd6008a89", @"/Views/_ViewImports.cshtml")]
    public class Views_Library_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Library\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
            BeginContext(43, 20, true);
            WriteLiteral("\r\n<h2>Index</h2>\r\n\r\n");
            EndContext();
            BeginContext(64, 45, false);
#line 8 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Library\Index.cshtml"
Write(Html.ActionLink("Add Book", "AddBook","Book"));

#line default
#line hidden
            EndContext();
            BeginContext(109, 14, true);
            WriteLiteral("\r\n\r\n<hr />\r\n\r\n");
            EndContext();
            BeginContext(124, 45, false);
#line 12 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Library\Index.cshtml"
Write(Html.ActionLink("Add User", "AddUser","User"));

#line default
#line hidden
            EndContext();
            BeginContext(169, 14, true);
            WriteLiteral("\r\n\r\n<hr />\r\n\r\n");
            EndContext();
            BeginContext(184, 48, false);
#line 16 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Library\Index.cshtml"
Write(Html.ActionLink("List of books", "Books","Book"));

#line default
#line hidden
            EndContext();
            BeginContext(232, 16, true);
            WriteLiteral("\r\n\r\n\r\n<hr />\r\n\r\n");
            EndContext();
            BeginContext(249, 48, false);
#line 21 "C:\Users\Tijana\Desktop\Master\Repositories\Library\Library\Views\Library\Index.cshtml"
Write(Html.ActionLink("List of users", "Users","User"));

#line default
#line hidden
            EndContext();
            BeginContext(297, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
